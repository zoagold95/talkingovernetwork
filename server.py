import socket

def server():
    port = 6166                    # Reserve a port for your service.
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)             # Create a socket object
    host = socket.gethostname()     # Get local machine name
    s.bind((host, port))

    s.listen(1)
    c, addr = s.accept()
    print("Connection from: " + str(addr))
    while True:
        data = c.recv(1024).decode('utf-8')
        if not data:
            break
        print('From online user: ' + data)
        data = data.upper()
        c.send(data.encode('utf-8'))
    c.close()

if __name__ == '__main__':
    server()
